#!/bin/bash
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "This script creates a Express job configuration file"
   echo "that can later be executed using cmsRun. By default,"
   echo "the command uses the file pointed by env variable"
   echo "T0_API_JSON_FILENAME, but the user can provide a"
   echo "different path. The command can also use the production"
   echo "configuration for an specific run and stream."
   echo
   echo "Syntax: expressWrapper.sh [-r|j|h]"
   echo "options:"
   echo "-r    User provides the run number and stream name "
   echo "      separated by a colon.Creates an Express configuration"
   echo "      that mimics the one used in product."
   echo "      usage: expressWrapper.sh -r <run_number>:<stream_name> lfn"
   echo ""
   echo "-j    User provides a JSON file containing the"
   echo "      desired configuration, i. e. GT, scenario, etc"
   echo "      Visit this url foran example JSON: https://cmsweb.cern.ch/t0wmadatasvc/prod/express_config?run=322963&stream=Calibration"
   echo "      usage: expressWrapper.sh -j <path_to_json> lfn"
   echo ""
   echo "-s    User provides a SCRAM architecture to override the"
   echo "      configuration."
   echo "      usage: repackWrapper.sh -r <run_number> -s <scram_arch> lfn"
   echo ""
   echo "-h    Prints this help message"
   echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################

# Load libraries
source lib/t0api.bash
source lib/utils.bash

if [ "$#" -eq 0 ]; then
    echo -e "Running script without parameters.\n Displaying Help:\n\n"
    Help
    exit
fi

# Get the options
while getopts ":hj:r:s:" option; do
    case $option in
        r) # Get JSON from t0WMADataSvc
            if [[ ! "$OPTARG" =~ [0-9]{6}:[a-zA-Z0-9]+ ]]; then
                echo -e "Wrong run:stream format\n";
                Help;
                exit;
            fi
            IFS=":" read -r -a arr <<< "$OPTARG" # Separates run and stream by colon
            run_num=${arr[0]}; stream_name=${arr[1]};
            get_express_config "$run_num" "$stream_name"|| { echo Error accessing T0 API; exit;};;
        j) # User provided JSON
            T0_API_JSON_FILENAME=$OPTARG;;
        s) # User provided SCRAM arch
            user_scram=$OPTARG;;
        h) # display Help
            Help
            exit;;
        \?) # Invalid option
            echo "Error: Invalid option"
            Help
            exit;;
   esac
done

# Last argument should be lfn
lfn="${*: -1}"

# Load JSON file into configMap array
load_express_config "$T0_API_JSON_FILENAME" || { echo Error loading config; exit;}

CMSSW="${configMap['reco_cmssw']}"
SCRAM_ARCH="${configMap['reco_scram_arch']}"
[ -n "${user_scram}" ] && SCRAM_ARCH=$user_scram

# Create CMSSW area ccording to config
create_cmssw_env "${SCRAM_ARCH}" "${CMSSW}"

# Move to src folder. Exit otherwise
cd "${CMSSW}/src/"  || { echo "Unable to cd into CMSSW dir"; exit; }

#source cms environment variables
eval "$(scramv1 runtime -sh)"

# Get supported python command
command=$(get_python_command "${CMSSW}" || { echo "Unable to get Python command for ${CMSSW}"; exit;}) 

# Multicore in tests is supported by an eterogenous set of releases. Check this before adding it to the options PSet
if  version_supports_multicore "${CMSSW}" ; then
    echo "Using nThreads"
    options="--nThreads=${configMap['multicore']}"
else
    options=""
fi


if [ "${configMap['alca_skim']}" != 'null' ]
then
    
    options="$options --alcarecos=${configMap['alca_skim']}"
fi 

echo "Running command: $command $CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunExpressProcessing.py\
    --scenario=${configMap['scenario']} --raw --reco --fevt --dqm\
    --global-tag ${configMap['global_tag']} --lfn=$lfn $options"

"$command" "$CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunExpressProcessing.py"\
    --scenario="${configMap['scenario']}" --raw --reco --fevt --dqm\
    --global-tag "${configMap['global_tag']}" --lfn="$lfn" "$options" || { echo "Unable to create PSet"; exit 1;}

echo -e "\nYou created a Tier-0 Express PSet. To run the job, execute:\n"
echo -e "\tcd $(pwd)"
echo -e "\tcmsenv"
echo -e "\tcmsRun -e RunExpressProcessingCfg.py"
echo -e "\nGood luck!"