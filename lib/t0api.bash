#!/bin/bash
############################################################
# T0 API lib                                               #
############################################################
#
# This set of functions simplify access to T0 API Endpoints
# to be used in T0 Wrapper scripts. This library requires 
# that X509 env variables point to a valid proxy or cert/key
#
# All queries to API are persisted in a file with path T0_API_JSON_FILENAME


declare -A configMap
if [ -z "${T0_API_JSON_FILENAME}" ];then
    T0_API_JSON_FILENAME="config.json"
fi
T0_API_BASE_URL="https://cmsweb.cern.ch/t0wmadatasvc/prod/"

#######################################
# Get credentials from X509 env variables
# Globals:
#   X509_USER_PROXY, X509_USER_KEY, 
#   X509_USER_CERT
# Arguments:
#   None
# Outputs:
#   Writes credentials for curl. Proxy if 
#   possible, cert+key otherwise
#######################################
get_credentials() {
    if [ -n "${X509_USER_PROXY}" ]; then
        echo "--cert ${X509_USER_PROXY}"
    elif [[ -n "${X509_USER_KEY}" && -n "${X509_USER_CERT}" ]]; then
        echo "--cert ${X509_USER_CERT} --key ${X509_USER_KEY}"
    fi
}

curl_request() {

    creds="$(get_credentials)"
    if [ -z "${creds}" ]; then
        echo "No credentials found"
        return 1
    fi
    url=$1
    if [ -z "${url}" ]; then
        echo "No URL"
        return 1
    fi

    echo "URL: $url"
    curl -k $creds "$url" > "$T0_API_JSON_FILENAME" || { echo curl failed;return 1;}

    if [ "$(jq -r '.result[0]' < $T0_API_JSON_FILENAME)" = "null" ]; then
        echo "$T0_API_JSON_FILENAME is empty"
        return 1
    fi
}


#######################################
# Get express configuration for all streams of the
# latest run. Parameters can specify a runnumber and
# stream
# Globals:
#   X509_USER_PROXY, X509_USER_KEY, 
#   X509_USER_CERT, T0_API_JSON_FILENAME
# Arguments:
#   Runnumber (optional)
#   Stream (optional)
# Outputs:
#   Writes express configuration into T0_API_JSON_FILENAME
#######################################
get_express_config() {

    url="${T0_API_BASE_URL}express_config"
    # Check runnumber exist
    if [ -n "${1}" ]; then
        url="${url}?run=${1}"
    fi
    # Check if stream parameter exist
    if [ -n "${2}" ]; then
        url="${url}&stream=${2}"
    fi

    curl_request "$url" || { echo "Unable to retrieve Express config"; return 1; }

}

#######################################
# Get reco configuration for all PDs of the
# latest run. Parameters can specify a runnumber and
# PD
# Globals:
#   X509_USER_PROXY, X509_USER_KEY, 
#   X509_USER_CERT, T0_API_JSON_FILENAME
# Arguments:
#   Runnumber (optional)
#   primary_dataset (optional)
# Outputs:
#   Writes express configuration into T0_API_JSON_FILENAME
#######################################
get_reco_config() {

    url="${T0_API_BASE_URL}reco_config"
    # Check runnumber exist
    if [ -n "${1}" ]; then
        url="${url}?run=${1}"
    fi
    # Check if stream parameter exist
    if [ -n "${2}" ]; then
        url="${url}&primary_dataset=${2}"
    fi

    curl_request "$url" || { echo "Unable to retrieve Reco config"; return 1; }
}

#######################################
# Load repack config from json file
# 
# Globals:
# Arguments:
#   path_to_json (optional)
# Outputs:
#   Populates global variable configMap
#######################################
load_repack_config() {
    if [ -n "${1}" ]; then
        T0_API_JSON_FILENAME="${1}"
    fi
    if [ ! -f "$T0_API_JSON_FILENAME" ]; then
        echo "Unable to read $T0_API_JSON_FILENAME"
        return 1
    fi

    configMap['scram_arch']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].scram_arch')
    configMap['cmssw']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].cmssw')
    print_config
}

#######################################
# Load express config from json file
# 
# Globals:
# Arguments:
#   path_to_json (optional)
# Outputs:
#   Populates global variable configMap
#######################################
load_express_config() {
    if [ -n "${1}" ]; then
        T0_API_JSON_FILENAME="${1}"
    fi
    if [ ! -f "$T0_API_JSON_FILENAME" ]; then
        echo "Unable to read $T0_API_JSON_FILENAME"
        return 1
    fi

    configMap['run']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].run')
    configMap['stream']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].stream')
    configMap['scram_arch']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].scram_arch')
    configMap['cmssw']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].cmssw')
    configMap['reco_scram_arch']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].reco_scram_arch')
    configMap['reco_cmssw']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].reco_cmssw')
    configMap['scenario']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].scenario')
    configMap['global_tag']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].global_tag')
    configMap['alca_skim']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].alca_skim' | sed -e 's/,/+/g')
    configMap['physics_skim']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].physics_skim' | sed -e 's/,/+/g')
    configMap['dqm_seq']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].dqm_seq' | sed -e 's/,/+/g')
    configMap['multicore']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].multicore')
    print_config
}

#######################################
# Load reco config from json file
# 
# Globals:
# Arguments:
#   path_to_json (optional)
# Outputs:
#   Populates global variable configMap
#######################################
load_reco_config() {
    if [ -n "${1}" ]; then
        T0_API_JSON_FILENAME="${1}"
    fi
    if [ ! -f "$T0_API_JSON_FILENAME" ]; then
        echo "Unable to read $T0_API_JSON_FILENAME"
        return 1
    fi

    configMap['run']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].run')
    configMap['primary_dataset']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].primary_dataset')
    configMap['reco_scram_arch']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].scram_arch')
    configMap['reco_cmssw']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].cmssw')
    configMap['scenario']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].scenario')
    configMap['global_tag']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].global_tag')
    configMap['alca_skim']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].alca_skim' | sed -e 's/,/+/g')
    configMap['physics_skim']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].physics_skim' | sed -e 's/,/+/g')
    configMap['dqm_seq']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].dqm_seq' | sed -e 's/,/+/g')
    configMap['multicore']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].multicore')
    configMap['write_nanoaod']=$(cat $T0_API_JSON_FILENAME | jq -r '.result[0].write_nanoaod')
    
    print_config
}


#######################################
# Load Expres/Reco config from json file.
# 
# Globals:
# Arguments:
#   path_to_json (optional)
# Returns:
#   0 if express, 1 if reco
#######################################
load_config() {
    if [ -n "${1}" ]; then
        T0_API_JSON_FILENAME="${1}"
    fi
    if [ "$(jq -r '.result[0].primary_dataset' < $T0_API_JSON_FILENAME)" = "null" ]; then
        load_express_config "$T0_API_JSON_FILENAME"
        export is_reco=0
        return
    fi
    load_reco_config "$T0_API_JSON_FILENAME"
    export is_reco=1
}

#######################################
# Print configuration associative array
# 
# Globals:
#   configMap
# Arguments:
# Outputs:
#   Prints config to stdout
#######################################
print_config() {
    echo "Loaded configuration:"
    for x in "${!configMap[@]}"; do 
        printf "\tconfigMap[%s]=%s\n" "$x" "${configMap[$x]}"
    done
}