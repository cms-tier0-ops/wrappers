#!/bin/bash
############################################################
# Utilities for wrappers lib                               #
############################################################
#
# This set of functions simplify access requered for the 
# correct operations of T0 wrappers

ERROR_INVALID_CMSSW_VERSION=20
ERROR_MULTICORE_NOT_SUPPORTED=21
ERROR_NOT_ENOUGH_ARGS=22

############################################################
# Provides the python command to use with the specified 
# CMSSW version
# Arguments:
#   CMSSW Version
# Outputs:
#   prints python command
############################################################
get_python_command() {
    if [[ $1 != CMSSW_* ]]; then
        return $ERROR_INVALID_CMSSW_VERSION
    fi

    if [[ "$1" > "CMSSW_12_" ]]
    then
        # Using Python3 CMSSW version
        echo "python3"
    else
        #Using Python2 CMSSW version
        echo "python"
    fi
}

############################################################
# Returns 0 if the CMSSW version supports multicore tests
# CMSSW version
# Arguments:
#   CMSSW Version
# Return:
#   0 if multicore is supported
#   1 otherwise
############################################################
version_supports_multicore() {
    if [[ $1 != CMSSW_* ]]; then
        echo "$1 is not a valid CMSSW version"
        return $ERROR_INVALID_CMSSW_VERSION
    fi

    if [[ $1 > "CMSSW_11_" && $1 < "CMSSW_13_3_" ]]
    then
        # Test scripts from this release support multicore
        return 0
    else
        # Test scripts from this release DON'T
        return $ERROR_MULTICORE_NOT_SUPPORTED
    fi
}

############################################################
# Creates a CMSSW area with the specified scram architecture
# and CMSSW version
# Arguments:
#   SCRAM Architecture
#   CMSSW Version
# Return:
#   0 if succesful
#   1 otherwise
############################################################
create_cmssw_env() {
    if [ $# -ne 2 ]; then
        echo "Not enough arguments to create CMSSW env"
        return "$ERROR_NOT_ENOUGH_ARGS"
    fi
    if [[ $2 != CMSSW_* ]]; then
        echo "$2 is not a valid CMSSW version"
        return $ERROR_INVALID_CMSSW_VERSION
    fi
    #source cmsset values
    source /cvmfs/cms.cern.ch/cmsset_default.sh

    # define architecture as requested by user
    export SCRAM_ARCH="$1"

    #create the project with CMSSW version given by the user
    scramv1 project CMSSW "${2}"
}