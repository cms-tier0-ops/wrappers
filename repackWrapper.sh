#!/bin/bash
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "This script creates a Repack job configuration file"
   echo "that can later be executed using cmsRun. By default,"
   echo "the command uses the file pointed by env variable"
   echo "T0_API_JSON_FILENAME, but the user can provide a"
   echo "different path. The command can also use the production"
   echo "configuration for an specific run."
   echo
   echo "Syntax: repackWrapper.sh [-r|j|h] lfn"
   echo "options:"
   echo "-r    User provides the run number and stream name."
   echo "      Creates a Repack configuration that mimicks"
   echo "      the one used in product."
   echo "      usage: repackWrapper.sh -r <run_number> lfn"
   echo ""
   echo "-j    User provides a JSON file containing the"
   echo "      desired configuration, i. e. GT, scenario, etc"
   echo "      Visit this url foran example JSON: https://cmsweb.cern.ch/t0wmadatasvc/prod/express_config?run=322963&stream=Calibration"
   echo "      usage: repackWrapper.sh -j <path_to_json> lfn"
   echo ""
   echo "-s    User provides a SCRAM architecture to override the"
   echo "      configuration."
   echo "      usage: repackWrapper.sh -r <run_number> -s <scram_arch> lfn"
   echo ""
   echo "-h    Prints this help message"
   echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################

source lib/t0api.bash
source lib/utils.bash

if [ "$#" -eq 0 ]; then
    echo -e "Running script without parameters.\n Displaying Help:\n\n"
    Help
    exit
fi

# Get the options
while getopts ":hr:j:s:" option; do
    case $option in
        r) # Get JSON from t0WMADataSvc
            run_num=$OPTARG
            get_express_config "$run_num"|| { echo Error accessing T0 API; exit;};;
        j) # User provided JSON
            T0_API_JSON_FILENAME=$OPTARG;;
        s) # User provided SCRAM arch
            user_scram=$OPTARG;;
        h) # display Help
            Help
            exit;;
        \?) # Invalid option
            echo "Error: Invalid option"
            Help
            exit;;
   esac
done

# Last argument should be lfn
lfn="${*: -1}"

# Load JSON file into configMap array
load_repack_config "$T0_API_JSON_FILENAME" || { echo Error loading config; exit;}

#source cmsset values
source /cvmfs/cms.cern.ch/cmsset_default.sh

# define architecture as requested by user
export SCRAM_ARCH "${configMap['scram_arch']}"
[ -n "${user_scram}" ] && SCRAM_ARCH=$user_scram

#create the project with CMSSW version given by the user
scramv1 project CMSSW "${configMap['cmssw']}"

# Move to src folder. Exit otherwise
cd "${configMap['cmssw']}/src/"  || { echo "Unable to cd into CMSSW dir"; exit; }

#source cms environment variables
eval "$(scramv1 runtime -sh)"

echo "CMSSW version: ${configMap['cmssw']}"
command=$(get_python_command "${configMap['cmssw']}" || { echo "Unable to get Python command for ${configMap['cmssw']}"; exit;}) 


echo "Running command: $command $CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunRepack.py --lfn=$lfn"
"$command" "$CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunRepack.py" "--lfn=$lfn"  || { echo "Unable to create PSet"; exit 1;}

echo -e "\nYou created a Tier-0 repack PSet. To run the job, execute:\n"
echo -e "\tcd $(pwd)"
echo -e "\tcmsenv"
echo -e "\tcmsRun -e RunRepackCfg.py"
echo -e "\nGood luck!"