#!/bin/bash
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "This script creates a AlCa Skimming job configuration file"
   echo "that can later be executed using cmsRun. It can take a"
   echo "Express or Reco configuration."
   echo
   echo "Syntax: AlCaSkimWrapper.sh [-r|j|h|s]"
   echo "options:"
   echo "-r    User provides the run number and pd/stream name."
   echo "      Creates aconfiguration that mimics the one used"
   echo "      in product."
   echo "      usage: AlCaSkimWrapper.sh -r <run_number>:<primary_dataset/stream> lfn"
   echo ""
   echo "-j    User provides a JSON file containing the"
   echo "      desired configuration, i. e. GT, scenario, etc"
   echo "      Visit this url for an example JSON: https://cmsweb.cern.ch/t0wmadatasvc/prod/reco_config?run=322963&primary_dataset=ZeroBias"
   echo "      usage: AlCaSkimWrapper.sh -j <path_to_json> lfn"
   echo ""
   echo "-s    User provides a SCRAM architecture to override the"
   echo "      configuration."
   echo "      usage: AlCaSkimWrapper.sh -s <scram_arch> lfn"
   echo ""
   echo "-h    Prints this help message"
   echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################

# Load libraries
source lib/t0api.bash
source lib/utils.bash

if [ "$#" -eq 0 ]; then
    echo -e "Running script without parameters.\n Displaying Help:\n\n"
    Help
    exit
fi

# Get the options
while getopts ":hj:r:s:" option; do
    case $option in
        r) # Get JSON from t0WMADataSvc
            if [[ ! "$OPTARG" =~ [0-9]{6}:[a-zA-Z0-9]+ ]]; then
                echo -e "Wrong run:PD format\n";
                Help;
                exit;
            fi
            IFS=":" read -r -a arr <<< "$OPTARG" # Separates run and stream by colon
            run_num=${arr[0]}; 
            primary_dataset=${arr[1]};
            get_reco_config "$run_num" "$primary_dataset"||\
            get_express_config "$run_num" "$primary_dataset" || { echo Error accessing T0 API; exit;};;
        j) # User provided JSON
            T0_API_JSON_FILENAME=$OPTARG;;
        s) # User provided SCRAM arch
            user_scram=$OPTARG;;
        h) # display Help
            Help
            exit;;
        \?) # Invalid option
            echo "Error: Invalid option"
            Help
            exit;;
   esac
done

# Last argument should be lfn
lfn="${*: -1}"

# Load JSON file into configMap array. Can be reco or express config
load_config "$T0_API_JSON_FILENAME" || { echo Error loading config; exit;}

if [ "${configMap['alca_skim']}" == 'null' ];then
    echo "No AlCa skims in this configuration"; exit 1
fi

CMSSW="${configMap['reco_cmssw']}"
SCRAM_ARCH="${configMap['reco_scram_arch']}"

# If user user provided scram arch, use that
[ -n "${user_scram}" ] && SCRAM_ARCH=$user_scram

# Create CMSSW area ccording to config
create_cmssw_env "${SCRAM_ARCH}" "${CMSSW}"

# Move to src folder. Exit otherwise
cd "${CMSSW}/src/"  || { echo "Unable to cd into CMSSW dir"; exit; }

#source cms environment variables
eval "$(scramv1 runtime -sh)"

# Get supported python command
command=$(get_python_command "${CMSSW}" || { echo "Unable to get Python command for ${CMSSW}"; exit;}) 

#
configMap['alca_skim']=${configMap['alca_skim']//+/,}

echo "Running command: $command $CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunAlcaSkimming.py\
    --scenario=${configMap['scenario']} --global-tag ${configMap['global_tag']}\
     --lfn=$lfn --skims=${configMap['alca_skim']}"

$command "$CMSSW_RELEASE_BASE/src/Configuration/DataProcessing/test/RunAlcaSkimming.py"\
    --scenario="${configMap['scenario']}" --global-tag "${configMap['global_tag']}"\
     --lfn="$lfn" --skims="${configMap['alca_skim']}"

echo -e "\nYou created a Tier-0 AlCa Skimming PSet. To run the job, execute:\n"
echo -e "\tcd $(pwd)"
echo -e "\tcmsenv"
echo -e "\tcmsRun -e RunAlcaSkimmingCfg.py"
echo -e "\nGood luck!"